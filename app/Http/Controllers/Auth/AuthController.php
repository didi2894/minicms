<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use MandrillMail;
use Session;
use App\Marketing;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    //Na registratie naar home redirecten
    protected $redirectPath = '/auth/register';

    //Na login naar home redirecten
    protected $loginPath = '/';

    //Na logout naar root redirecten (index)
    protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'failed' => 'Het e-mailadres en wachtwoord komen niet overeen.',
            'throttle' => 'Te veel inlogpogingen, probeer opnieuw in :seconds seconden.',
            'name.required' => 'Gebruikersnaam is verplicht.',
            'name.max' => 'Gebruikersnaam mag max :max karakters bevatten',
            'name.unique' => 'Gebruikersnaam bestaat al.',
            'email.required' => 'E-mailadres is verplicht.',
            'email.max' => 'E-mailadres mag max :max karakters bevatten',
            'email.unique' => 'E-mailadres bestaat al.',
            'password.required' => 'Paswoord is verplicht.',
            'password.confirmed' => 'Paswoorden komen niet overeen.',
            'password.min' => 'Paswoord moet minstens :min karakters bevatten.',
        ];

        return Validator::make($data, [
            'name' => 'required|max:40|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ], $messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if (str_contains($data['unique_code'], "ABCDEF123")) {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'active' => 1,
            ]);
        } else {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'active' => 0,
            ]);
        }

    }

    public function postLogin(Request $request)
    {
        $messages = [
            'failed' => 'Het e-mailadres en wachtwoord komen niet overeen.',
            'throttle' => 'Te veel inlogpogingen, probeer opnieuw in :seconds seconden.',
            'email.required' => 'E-mailadres is verplicht.',
            'password.required' => 'Paswoord is verplicht',
        ];

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ], $messages);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }


        $credentials = $this->getCredentials($request);
        if (Auth::validate($credentials)) {

            $user = Auth::getLastAttempted();
            if ($user->active) {
                Auth::login($user, $request->has('remember'));

            } else {
                Auth::login($user, $request->has('remember'));
                return redirect($this->loginPath());

            }
        }
        else{
            return redirect()->intended($this->redirectPath())
            ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'active' => 'Je account moet geactiveerd zijn om in te loggen.'
                ]);
        }

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if (str_contains($request['unique_code'], "ABCDEF123")) {
            Auth::login($this->create($request->all()));
        } else {
            $this->create($request->all());
            Session::flash('success', 'Succesvol geregistreerd!');
        }


        return redirect($this->redirectPath());
    }
}
