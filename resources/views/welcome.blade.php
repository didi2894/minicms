<!DOCTYPE html>
<html>
    <head>
        <title>Registration</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,400" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{asset('css/screen.css')}}">
    </head>
    <body>
        @include('partials.navigation')

        <div class="container">
            @include('partials.errors')

            @if (Auth::check())
                <h3>Welkom, {{ Auth::user()->name}}</h3>

            @else
                <h3>Welcome op deze blog.</h3>
                <h3>Log in om artikels te posten</h3>
            @endif
                <div class="content panel panel-primary">
                    @foreach($allcontent as $c)
                        <div class="col-md-4">
                            <h6><a target="_blank" href="{{$c->url}}">{{$c->url}}</a></h6>

                                @if(strpos($c->url,'youtube') !== false)
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$c->url_info}}" frameborder="0" allowfullscreen></iframe>
                                @elseif(strpos($c->url,'vimeo') !== false)
                                    <iframe src="https://player.vimeo.com/video/{{$c->url_info}}" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                @elseif(strpos($c->url,'soundcloud') !== false)

                                <?php
                                //Get the SoundCloud URL
                                $url = $c->url;
                                //Get the JSON data of song details with embed code from SoundCloud oEmbed
                                $getValues=file_get_contents('http://soundcloud.com/oembed?format=js&url='.$url.'&iframe=true');
                                //Clean the Json to decode
                                $decodeiFrame=substr($getValues, 1, -2);
                                //json decode to convert it as an array
                                $jsonObj = json_decode($decodeiFrame);

                                //Change the height of the embed player if you want else uncomment below line
                                // echo $jsonObj->html;
                                //Print the embed player to the page
                                echo str_replace('height="400"', 'height="140"', $jsonObj->html);
                                    ?>
                                @else

                                @endif
                            <h4>{{$c->user_name}}</h4>

                            <div class="comment_form"> <!--Add comment -->
                                {!! Form::open() !!}
                                {!! Form::text('body', null, ['class' => 'form-control comments', 'placeholder' => 'Uw reactie', 'autocomplete' => 'off']) !!}
                                {!! Form::submit('Reageer', ['class' => 'btn btn-primary form-control comments']) !!}
                                {!! Form::hidden('content_id', $c->id) !!}
                                {!! Form::close() !!}
                            </div>


                            @foreach ($allcomments as $com)
                                @if($com->on_content == $c->id)
                                        <ul class="list-group">
                                            <li class="list-group-item">{{$com->body}}</li>
                                        </ul>
                                @else
                                @endif
                            @endforeach
                        </div>
                    @endforeach


            </div>
        </div>
    </body>
</html>
