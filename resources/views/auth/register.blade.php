<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/screen.css')}}">
</head>
<body>
@include('partials.navigation')

<div class="container">
    <div class="content">

        @include('partials.errors')


        {!! Form::open(array('url' => 'auth/register')) !!}
        <div class="form-group">
            {!! Form::label('name', 'Username'); !!}
            {!! Form::text('name', $value = null, array('class' => 'form-control', 'placeholder' => 'Username')); !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'E-mail-adres'); !!}
            {!! Form::email('email', $value = null, array('class' => 'form-control', 'placeholder' => 'hello@gmail.com')); !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Paswoord'); !!}
            {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Pasword')); !!}
        </div>
        <div class="form-group">
            {!! Form::label('password_confirmation', 'Paswoord herhalen'); !!}
            {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Pasword confirmation')); !!}
        </div>
        <div class="form-group">
            {!! Form::label('unique_code', 'Unieke code (zie READ.ME in git)'); !!}
            {!! Form::password('unique_code', array('class' => 'form-control', 'placeholder' => 'qsflsjdfhQ4D')); !!}
        </div>


        <div class="form-group button__margin">
            <button class="btn btn-primary" type="submit">Register <i class="fa fa-angle-right"></i></button>
            <a class="register__link" href="login">Already have an account?</a>
        </div>
        {!! Form::close(); !!}
    </div>
</div>
</body>
</html>
