<!DOCTYPE html>
<html>
<head>
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/screen.css')}}">
</head>
<body>
@include('partials.navigation')

<div class="container">
    <div class="content">

        @include('partials.errors')


        {!! Form::open(array('url' => 'auth/login')) !!}
        <div class="form-group">
            {!! Form::label('email', 'E-mailadres'); !!}
            {!! Form::email('email', $value = null, array('class' => 'form-control', 'placeholder' => 'hello@gmail.com')); !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Paswoord'); !!}
            {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Paswoord')); !!}
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Login <i class="fa fa-angle-right"></i></button>
            <a class="register__link" href="register">Nog geen account?</a>
        </div>
        {!! Form::close(); !!}
    </div>
</div>
</body>
</html>
