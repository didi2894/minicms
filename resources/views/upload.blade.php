<!DOCTYPE html>
<html>
<head>
    <title>Upload</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('css/screen.css')}}">
</head>
<body>
@include('partials.navigation')

<div class="container">
    <div class="content">

        @include('partials.errors')

        {!! Form::open(array('url' => 'upload/add')) !!}
        <div class="form-group">
            {!! Form::label('url', 'URL'); !!}
            {!! Form::text('url', $value = null, array('class' => 'form-control', 'placeholder' => 'youtube.com')); !!}
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Uploaden <i class="fa fa-angle-right"></i></button>
        </div>
        {!! Form::close(); !!}

    </div>
</div>
</body>
</html>
