<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav  navbar-nav navbar-left ">
                <li><a href="../">Home</a></li>
                @if (Auth::check())
                    <li><a href="{{ URL::to('upload') }}">Artikel uploaden</a></li>
                    <li><a href="{{ URL::to('auth/logout') }}">Afmelden</a></li>
                @else
                    <li><a href="{{ URL::to('auth/login') }}">Log in</a></li>
                    <li><a href="../auth/register">Registreren</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
